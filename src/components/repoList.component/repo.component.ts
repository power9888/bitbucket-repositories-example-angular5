import { Component, OnInit } from '@angular/core';
import {RepoDataService} from './data.service';
import { DataItem } from '../../models/DataItem';
import { SortDirection } from '../../models/SortDirection';

@Component({
  selector: 'repo-list',
  templateUrl: './repo.component.html',
  styleUrls: ['./repo.component.css'],
  providers: [RepoDataService],
})
export class RepoListComponent implements OnInit {
  title = 'bitbucket-data';
  repos: Array<DataItem>;
  sortDirections: Array<SortDirection>;
  currectDirecion: SortDirection;

  constructor (public repoDataService: RepoDataService) {
    this.repos = new Array<DataItem>();
  }

  ngOnInit() {    
    this.loadSortDirections();
    this.getRepoList();    
  }

  getRepoList() {
    this.repoDataService.getRepoList()
    .subscribe(res => {
      this.repos = this.repoDataService.toDataItem(res);   
    });
  }

  loadSortDirections() {
    this.sortDirections = new Array<SortDirection>();
    this.sortDirections.push(new SortDirection('owner_name', true));
    this.sortDirections.push(new SortDirection('repo_name', true));
    this.sortDirections.push(new SortDirection('html_link', true));
    this.sortDirections.push(new SortDirection('avatar_link', true));
    this.sortDirections.push(new SortDirection('created', true));
    this.currectDirecion = this.sortDirections[0];
  }

  sort(index) {
    this.sortDirections[index].direction = !this.sortDirections[index].direction;
    this.currectDirecion = this.sortDirections[index];
  }
}
