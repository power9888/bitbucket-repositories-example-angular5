import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IRepoResponse } from '../../models/IRepoResponse';
import { DataItem } from '../../models/DataItem';

@Injectable()

export class RepoDataService { 

    url:string = `https://api.bitbucket.org/2.0/repositories`;

    constructor(private http: HttpClient) {
    }

    //read from bitbucket api to get repositories
    getRepoList(): Observable<IRepoResponse> {
        return this.http.get<IRepoResponse>(this.url);
    }

    //convert IRepoResponse to DataItem
    toDataItem(repo: IRepoResponse): Array<DataItem> {
        let items = new Array<DataItem>();
        
        repo.values.map(data => {
            let item = new DataItem();
            item.owner_name = data.owner.display_name;
            item.repo_name = data.name;
            item.html_link = data.links.html.href;
            item.avatar_link = data.links.avatar.href;
            item.created = data.created_on;
            items.push(item);
        });
        
        return items;
    }
}