import {IRepo} from './IRepo';

export interface IRepoResponse {
    next: string;
    pagelen: number;
    values: IRepo[];
}