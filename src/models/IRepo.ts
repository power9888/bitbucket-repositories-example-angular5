import {IOwner} from './IOwner';
import {ILinks} from './ILinks';

export interface IRepo {
    created_on: Date;
    name: string;
    owner: IOwner;
    links: ILinks;
}