import {ILink} from './ILink';

export interface ILinks {
    avatar: ILink;
    html: ILink;
    self: ILink;
}