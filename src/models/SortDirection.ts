export class SortDirection {

    property: string;
    direction: boolean;

    constructor(_property: string, _direction: boolean) {
        this.property = _property;
        this.direction = _direction;
    }
}
