export class DataItem {
    owner_name: string;
    repo_name: string;
    html_link: string;
    avatar_link: string;
    created: Date;
}