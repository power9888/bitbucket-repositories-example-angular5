import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RepoListComponent } from '../components/repoList.component/repo.component';
import {RepoDataService} from '../components/repoList.component/data.service';
import { HttpClientModule } from '@angular/common/http';
import { OrderrByPipe } from '../pipes/OrderByPipe';


@NgModule({
  declarations: [
    AppComponent,
    RepoListComponent,
    OrderrByPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [RepoDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
