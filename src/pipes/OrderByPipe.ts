import { Pipe, PipeTransform } from '@angular/core';
@Pipe({  name: 'orderBy' })
export class OrderrByPipe implements PipeTransform {

    transform(records: Array<any>, args?: any): any {
        return records.sort(function(a, b){
            let direction = (args.direction) ? 1 : -1;
            if(a[args.property] < b[args.property]){
                return -1 * direction;
            }
            else if( a[args.property] > b[args.property]){
                return 1 * direction;
            }
            else{
                return 0;
            }
        });
    };
}
